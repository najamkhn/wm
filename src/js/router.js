var Backbone = require('backbone');
var HomeView = require('./views/home.view');
var ListView = require('./views/list.view');
var DetailsView = require('./views/details.view');

var Router = Backbone.Router.extend({
    routes: {
        '': 'default',
        'list': 'list',
        'list/:mode': 'list',
        'details/:id': 'details'
    },

    initialize: function() {
        Backbone.history.start();
    },

    default: function() {
        var view = new ListView('all');
    },

    list: function(mode) {
        var view = new ListView(mode || "all");
    },

    details: function(id) {
        var view = new DetailsView(id);
    }
});

module.exports = Router;
