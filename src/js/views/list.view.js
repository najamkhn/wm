var $ = require('jquery');
var ldsh = require('lodash');
var datatable = require('../datatable');
var moment = require('moment');
var Backbone = require('backbone');
var template = require('../templates/list.template.hbs');
var data = require('../data/challenge.json');

Backbone.$ = $;
Backbone._ = ldsh;

var ListView = Backbone.View.extend({
    el: '#main',
    template: template,

    initialize: function(mode) {
        this.render(mode);
        this.postRender(mode);
        return this;
    },

    postRender: function(mode) {

        // To reformat dates for human readable form
        $('.end-dates').each(function() {
            $(this).html(moment().format('MMMM DD, YYYY', $(this).html()));
            return this;
        });

        // To highlight the selected link
        $('[data-slug="' + mode + '"]').addClass('selected');

        // Sortable table plugin initialization
        $('#example').dataTable({
            "paging": false,
            "info": false,
            "searching": false
        });
    },

    render: function(mode) {
        var param = mode;
        this.projects = data.projects;

        // Translate active/inactive to true/false respectively as they are
        // stored in the data in that format and then grab the mode requested.
        // (i.e, all active/inactive projects)
        if (mode == 'active' || mode == 'inactive') {
            param = (mode == 'active') ? true : false;

            this.projects = ldsh.filter(data.projects, function(el) {
                if (el.active == param)
                    return el;
            });

        } else {
            this.projects = data.projects
        }

        this.$el.html(
            template({
                projects: this.projects
            })
        );
    }
});

module.exports = ListView;
