var $ = require('jquery');
var ldsh = require('lodash');
var moment = require('moment');
var Backbone = require('backbone');
var template = require('../templates/details.template.hbs');
var data = require('../data/challenge.json');

// Keeping local copies of them for Backbone
Backbone.$ = $;
Backbone._ = ldsh;

var DetailsView = Backbone.View.extend({
    el: '#main',
    template: template,

    initialize: function(id) {
        this.render(id);
        return this;
    },

    render: function(id) {
        var iID = parseInt(id),
            projectID = localStorage.getItem(id);

        // To pluck the `id` attribute and find out if the id provided in the
        // url parameters actually exists in the data or not.
        if (!projectID) {
            projectID = ldsh.pluck(data.projects, 'id').indexOf(iID);
        }

        this.project = data.projects[projectID]

        // Extending the `project` object so it would contain the proper date
        // format and the `id` for next and previous projects to navigate,
        // also calculating project progress for the progress bar.
        this.projectExtended = ldsh.extend(this.project, {
            progress: (this.project.current_step / this.project.total_steps) * 100,
            prev: iID - 1,
            next: iID + 1,
            start_date: moment().format('DD/MM/YYYY', this.project.start_date),
            end_date: moment().format('DD/MM/YYYY', this.project.end_date)
        });

        this.$el.html(
            template({
                "project": this.projectExtended
            })
        );
    }
});

module.exports = DetailsView;
