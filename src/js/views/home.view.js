var $ = require('jquery');
var ldsh = require('lodash');
var Backbone = require('backbone');
var template = require('../templates/home.template.hbs');

Backbone.$ = $;
Backbone._ = ldsh;

var HomeView = Backbone.View.extend({
    el: '#main',
    template: template,

    render: function() {
        list = []

        this.$el.html(
            template({
                projects: data.projects
            })
        );
    }
});

module.exports = HomeView;
