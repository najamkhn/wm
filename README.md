Setup
=========

To setup and run, just call the following command:

```
npm start
```

Features
====
- Filtering for active/inactive projects
- Sorting on column headers
- Navigate between projects
